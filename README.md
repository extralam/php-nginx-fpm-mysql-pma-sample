
* Quick summary
This is a sample PHP project.
Include php 7.1.2 + nginx + mariadb + phpmyadmin

* Version
1.0

* browser url
	* website : ip_address : 6080
	* pma : ip_address : 6081

### File Structure
- /docker
 - docker-compose.yml [plz change user pwd]
- /web
 - /conf
 - /nginx
    -- nginx-config.conf
 - [your php script]
- /db [nothing change]